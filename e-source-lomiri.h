/*
 * Copyright (C) 2015 Canonical Ltd.
 * Copyright (C) 2024 UBports Foundation
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Renato Araujo Oliveira Filho <renato.filho@canonical.com>
 *
 */

#ifndef E_SOURCE_LOMIRI_H
#define E_SOURCE_LOMIRI_H

#include <libedataserver/libedataserver.h>

/* Standard GObject macros */
#define E_TYPE_SOURCE_LOMIRI \
    (e_source_lomiri_get_type ())
#define E_SOURCE_LOMIRI(obj) \
    (G_TYPE_CHECK_INSTANCE_CAST \
    ((obj), E_TYPE_SOURCE_LOMIRI, ESourceLomiri))
#define E_SOURCE_LOMIRI_CLASS(cls) \
    (G_TYPE_CHECK_CLASS_CAST \
    ((cls), E_TYPE_SOURCE_LOMIRI, ESourceLomiriClass))
#define E_IS_SOURCE_LOMIRI(obj) \
    (G_TYPE_CHECK_INSTANCE_TYPE \
    ((obj), E_TYPE_SOURCE_LOMIRI))
#define E_IS_SOURCE_LOMIRI_CLASS(cls) \
    (G_TYPE_CHECK_CLASS_TYPE \
    ((cls), E_TYPE_SOURCE_LOMIRI))
#define E_SOURCE_LOMIRI_GET_CLASS(obj) \
    (G_TYPE_INSTANCE_GET_CLASS \
    ((obj), E_TYPE_SOURCE_LOMIRI, ESourceLomiriClass))

/**
 * E_SOURCE_EXTENSION_LOMIRI:
 *
 * Pass this extension name to e_source_get_extension() to access
 * #ESourceLomiri.  This is also used as a group name in key files.
 *
 **/
#define E_SOURCE_EXTENSION_LOMIRI "Lomiri"

G_BEGIN_DECLS

typedef struct _ESourceLomiri ESourceLomiri;
typedef struct _ESourceLomiriClass ESourceLomiriClass;
typedef struct _ESourceLomiriPrivate ESourceLomiriPrivate;

/**
 * ESourceLomiri:
 *
 * Contains only private data that should be read and manipulated using the
 * functions below.
 *
 **/
struct _ESourceLomiri {
    ESourceExtension parent;
    ESourceLomiriPrivate *priv;
};

struct _ESourceLomiriClass {
    ESourceExtensionClass parent_class;
};

GType           e_source_lomiri_get_type            (void) G_GNUC_CONST;

guint           e_source_lomiri_get_account_id      (ESourceLomiri *extension);
void            e_source_lomiri_set_account_id      (ESourceLomiri *extension,
                                                     guint id);

const gchar *   e_source_lomiri_get_application_id  (ESourceLomiri *extension);
gchar *         e_source_lomiri_dup_application_id  (ESourceLomiri *extension);
void            e_source_lomiri_set_application_id  (ESourceLomiri *extension,
                                                     const gchar *application_id);

gboolean        e_source_lomiri_get_autoremove      (ESourceLomiri *extension);
void            e_source_lomiri_set_autoremove      (ESourceLomiri *extension,
                                                     gboolean flag);

gboolean        e_source_lomiri_get_writable        (ESourceLomiri *extension);
void            e_source_lomiri_set_writable        (ESourceLomiri *extension,
                                                     gboolean flag);

const gchar *   e_source_lomiri_get_account_provider(ESourceLomiri *extension);
gchar *         e_source_lomiri_dup_account_provider(ESourceLomiri *extension);

const gchar *   e_source_lomiri_get_metadata        (ESourceLomiri *extension);
gchar *         e_source_lomiri_dup_metadata        (ESourceLomiri *extension);
void            e_source_lomiri_set_metadata        (ESourceLomiri *extension,
                                                     const gchar *metadata);

G_END_DECLS

#endif /* E_SOURCE_LOMIRI_H */
