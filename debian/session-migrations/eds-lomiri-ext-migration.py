#!/usr/bin/python3

import configparser
import pathlib
import xdg.BaseDirectory

# Returns whether this source is migrated.
def migrate_single_source(source_path: pathlib.Path):
    source = configparser.ConfigParser()
    source.read(source_path)

    if source.has_section('Lomiri'):
        print(f"{source_path.name}: already has 'Lomiri' section, don't migrate.")
        return False

    if not source.has_section('Ubuntu'):
        print(f"{source_path.name}: doesn't have 'Ubuntu', nothing to migrate.")
        return False

    # Turns out, you can just assign one section to another. Just like that.
    source['Lomiri'] = source['Ubuntu']

    with open(source_path, 'w') as fp:
        source.write(fp)

    return True

def main():
    print("Migrating EDS sources using Ubuntu extension to Lomiri extension")

    count_success = 0
    count_fail = 0
    eds_sources_dir = pathlib.Path(xdg.BaseDirectory.xdg_config_home) / "evolution" / "sources"

    for source_path in eds_sources_dir.iterdir():
        if source_path.suffix != ".source" or not source_path.is_file():
            print(f'Skiping {source_path.name}, not an EDS source')
            continue

        if migrate_single_source(source_path):
            count_success += 1
        else:
            count_fail += 1

    count_total = count_success + count_fail
    print(f'Attempted {count_total} source(s). '
          f'{count_success} source(s) migrated, {count_fail} source(s) not migrated.')

if __name__ == "__main__":
    main()
