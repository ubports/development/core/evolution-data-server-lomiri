cmake_minimum_required(VERSION 3.5)

project(evolution-data-server-lomiri VERSION 0.2.1 LANGUAGES C)

include(FindPkgConfig)

# Standard install paths
include(GNUInstallDirs)

pkg_check_modules(GLIB REQUIRED glib-2.0>=2.32)
pkg_check_modules(GIO REQUIRED gio-2.0>=2.32)
pkg_check_modules(EDATASERVER REQUIRED libedataserver-1.2>=3.8)
pkg_check_modules(FOLKS REQUIRED folks>=0.9.0)
pkg_check_modules(FOLKS_EDS REQUIRED folks-eds)
pkg_check_modules(GLIB_ACCOUNTS REQUIRED libaccounts-glib)

include_directories(
    ${CMAKE_BINARY_DIR}
    ${GLIB_INCLUDE_DIRS}
    ${GLIB_ACCOUNTS_INCLUDE_DIRS}
    ${GIO_INCLUDE_DIRS}
    ${FOLKS_EDS_INCLUDE_DIRS}
)

set(LOMIRI_SOURCE_LIB edataserver-source-lomiri)

execute_process(
    COMMAND pkg-config --variable=moduledir libebackend-1.2
    OUTPUT_VARIABLE EDS_MODULES_DIR
    OUTPUT_STRIP_TRAILING_WHITESPACE
)
set(LOMIRI_SOURCE_PC_FILE evolution-data-server-lomiri.pc)
configure_file(${LOMIRI_SOURCE_PC_FILE}.in ${CMAKE_CURRENT_BINARY_DIR}/${LOMIRI_SOURCE_PC_FILE} @ONLY)

# Lomiri EDS Source
set(LOMIRI_SOURCE_LIB_HEADERS
    e-source-lomiri.h
)
set(LOMIRI_SOURCE_LIB_SRCS
    e-source-lomiri.c
    ${LOMIRI_SOURCE_LIB_HEADERS}
)
add_library(${LOMIRI_SOURCE_LIB} SHARED
    ${LOMIRI_SOURCE_LIB_SRCS}
)
set_target_properties("${LOMIRI_SOURCE_LIB}" PROPERTIES
    VERSION 0.0.0 SOVERSION 0
)
target_link_libraries(${LOMIRI_SOURCE_LIB}
    ${GLIB_LIBRARIES}
    ${GLIB_ACCOUNTS_LIBRARIES}
    ${GIO_LIBRARIES}
    ${FOLKS_EDS_LIBRARIES}
)
install(TARGETS ${LOMIRI_SOURCE_LIB}
    LIBRARY DESTINATION ${CMAKE_INSTALL_FULL_LIBDIR}
)
install(FILES ${LOMIRI_SOURCE_LIB_HEADERS}
    DESTINATION ${CMAKE_INSTALL_FULL_INCLUDEDIR}/evolution-data-server-lomiri
)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${LOMIRI_SOURCE_PC_FILE}
    DESTINATION ${CMAKE_INSTALL_FULL_LIBDIR}/pkgconfig/
)

# Module Lomiri EDS Sources
set(MODULE_LOMIRI_SOURCES  module-lomiri-sources)
set(MODULE_LOMIRI_SOURCES_SRCS
    module-lomiri-sources.c
)
add_library(${MODULE_LOMIRI_SOURCES} MODULE
    ${MODULE_LOMIRI_SOURCES_SRCS}
)
set_target_properties(${MODULE_LOMIRI_SOURCES}
    PROPERTIES PREFIX ""
)
target_link_libraries(${MODULE_LOMIRI_SOURCES}
    ${GLIB_LIBRARIES}
    ${GLIB_ACCOUNTS_LIBRARIES}
    ${GIO_LIBRARIES}
    ${FOLKS_EDS_LIBRARIES}
    ${LOMIRI_SOURCE_LIB}
)
install(TARGETS ${MODULE_LOMIRI_SOURCES}
    LIBRARY DESTINATION ${EDS_MODULES_DIR}/
)
